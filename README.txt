*********************************
***Latex Vorlage für Bewerbung***
*********************************

Vorraussetzung für Benutzung dieser Latex-Vorlage
=================================================
Für Windows:
1. Vollständige Installation of Miktex
2. Cygwin Installation (mit "make")
3. Latex Editor z.B. Maktex

Für Linux:
1. Vollständige Installation für Latex
2. GCC make
3. Latex Editor z.B. Emacs

Verzeichnisstruktur
===================
.\Anschreiben (Deutsch)
.\Bewerbungsbild (Deutsch, English)
.\Lebenslauf (Deutsch)
.\CoverLetter (English)
.\ResumeCV (English)
.\Zeugnisse (Deutsch, English)

PDF für gesamte Bewerbungsunterlagen Erstellen
==============================================
Lass "make" in aktuellem Verzeichnis durchführen, default wird Bewerbung in Deutsch erstellt.
Danach wird eine zusammengefassende Datei "Bewerbungsunterlagen.pdf" in diesem Verzeichnis angelegt.

PDF für einzelnes Dokument in jedem Verzeichnis
===============================================
Z.B. für Lebenslauf.pdf
Die folgende Befehle durchführen
1. cd ./Lebenslauf 
2. make

Danach wird Lebenslauf.pdf in ./Lebenslauf angelegt.


Bewerbung in Deutschland
========================

1. make oder make deutsch

Bewerbung in Deutschland sind normalerweise Anschreiben, Lebenslauf, Zeugnisse benötig.
Als Default wird "make" in root Verzeichnis die Bewerbungsunterlagen.pdf, ein zusammengefassendes Dokument, in Deutsch erstellen.


Bewerbung bei Englishe Firmen
=============================

1. make english

Bei englishe Firmen sind normalerweise CoverLetter (für Motivation), Resume (oder CV), Zeugnisse erforderlich.
In dem Fall muss "make" jeweils in Verzeichnis ./CoverLetter und ./ResumeCV durchgeführt werden, um die PDFs zu erstellen.
Für eine zusammengefassende Datei in English muss man selbe das Makefile anpassen, danach "make" durchführen.

