#viewer = /cygdrive/C/Program\ Files\ \(x86\)/Foxit\ Software/Foxit\ Reader/Foxit\ Reader.exe
#viewer = /cygdrive/C/Program\ Files\ \(x86\)/Adobe/Reader\ 10.0/Reader/AcroRd32.exe
#viewer = evince

# Main file name and literature list
DEUTSCH_TEX = bewerbungsunterlagen.tex
ENGLISH_TEX = application.tex

latex = pdflatex

SRC = $(shell basename $(DEUTSCH_TEX) .tex)

date=$(shell date +%Y%m%d%H%M)

PDF = $(SRC).pdf

# was wird gemacht, falls nur make aufgerufen wird
#hier sollte noch der aspell check rein für jedes file einzeln über for schleife

deutsch: 
	make -C Anschreiben
	make -C Lebenslauf
	$(latex) $(DEUTSCH_TEX)
	$(clean)

english:
	make -C CoverLetter
	make -C ResumeCV
	$(latex) $(ENGLISH_TEX)
	$(clean)


clean: 
	@rm -f $(SRC).4ct $(SRC).4tc $(SRC).aux $(SRC).bbl $(SRC).blg $(SRC).brf $(SRC).code $(SRC).dvi $(SRC).err $(SRC).glo $(SRC).gls $(SRC).hp \
	$(SRC).idv $(SRC).lo? $(SRC).log $(SRC).lot $(SRC).out $(SRC).tmp $(SRC).toc $(SRC).tpt $(SRC).lbl $(SRC).idx \
	$(SRC).ilg $(SRC).ind $(SRC).ps $(SRC).xref $(SRC).code $(SRC).html $(SRC).css $(SRC).lg


mrproper: clean
	rm -f *~

